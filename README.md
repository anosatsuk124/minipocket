# Minipocket

Minipocket is a simple and lightweight bookmark and read-later manager.

## Basic Features

-   [ ] Cross-platform

    -   [ ] macOS
    -   [ ] Windows
    -   [ ] Linux
    -   [ ] Android
    -   [ ] iOS
    -   [ ] Web (if it's possible, but not a priority development)

-   [ ] Bookmark and read-later

-   [ ] Offline Reading

    -   [ ] Download web pages, images, and other resources

## License information

```license
Minipocket - A simple and lightweight bookmark and read-later manager.
Copyright (c) 2023 Satsuki Akiba <anosatsuk124@gmail.com> .

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
```
