use super::navbar::Navbar;
use super::navbar::NavigationItem;
use super::settings::Settings;
use dioxus::prelude::*;

pub fn Home(cx: Scope) -> Element {
    // pub struct NavigationItem<'a> {
    //     pub icon: Element<'a>,
    //     pub label: String,
    //     pub route: String,
    // }
    //
    // #[derive(Props)]
    // pub struct NavigationItems<'a> {
    //     pub items: Vec<NavigationItem<'a>>,
    // }
    let item = NavigationItem {
        icon: render!( span { class: "material-symbols-outlined", "delete" } ),
        label: "Delete".to_string(),
        route: "/delete".to_string(),
    };

    let items = vec![item];

    render!(
        Navbar { items: items }
        Settings {}
    )
}
