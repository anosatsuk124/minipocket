use dioxus::prelude::*;
use dioxus_router::prelude::*;

use crate::app::router::Route;
use crate::APP_NAME;

pub struct NavigationItem<'a> {
    pub icon: Element<'a>,
    pub label: String,
    pub route: String,
}

#[derive(Props)]
pub struct NavigationItems<'a> {
    pub items: Vec<NavigationItem<'a>>,
}

pub fn NavigationLists() {
    // for item in cx.props.items.iter() {
    //     div {
    //         class: "navbar bg-base-100",
    //
    //         onclick: move |_| {
    //         // TODO: impelements navigation
    //             log::info!("Navigating to {}", item.route);
    //             // cx.(item.route.clone());
    //         },
    //         item.icon.clone()
    //         label { item.label.clone() }
    //     }
    // }
    //
}

pub fn Navbar<'a>(cx: Scope<'a, NavigationItems<'a>>) -> Element<'a> {
    render!(
        div { class: "navbar bg-base-100",
            div { class: "flex-none",
                button { class: "btn btn-square btn-ghost", span { class: "material-symbols-outlined", "menu" } }
            }
            div {
                div { class: "flex-1",
                    Link { class: "btn btn-ghost normal-case text-xl text-xl", to: Route::Home {}, APP_NAME }
                }
            }
        }
    )
}
