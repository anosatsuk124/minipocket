use dioxus::prelude::*;
use dioxus_std::{i18n::use_i18, translate};

pub fn Settings(cx: Scope) -> Element {
    let i18 = use_i18(cx);

    render!(
        h1 { translate!(i18, "settings.settings") }
        LanguageSettings {}
    )
}

fn LanguageSettings(cx: Scope) -> Element {
    let i18 = use_i18(cx);

    let change_to_english = move |_| i18.set_language("en-US".parse().unwrap());
    let change_to_japanese = move |_| i18.set_language("ja-JP".parse().unwrap());

    render!(
        div {
            button { class: "btn", onclick: change_to_english, label { "English" } }
            button { class: "btn", onclick: change_to_japanese, label { "日本語" } }
        }
    )
}
