use dioxus_std::i18n::Language;
use std::str::FromStr;
use unic_langid::LanguageIdentifier;

static EN_US: &str = include_str!("./en-US.json");
static JA_JP: &str = include_str!("./ja-JP.json");

pub fn get_langcode() -> LanguageIdentifier {
    let initial_langcode: LanguageIdentifier;

    if cfg!(debug_assertions) {
        let lang_code_env = std::env::var("LANG_CODE").unwrap_or_default();

        if lang_code_env.is_empty() {
            initial_langcode = "en-US".parse().unwrap();
        } else {
            initial_langcode = lang_code_env.as_str().parse().unwrap();
        }
    } else {
        // TODO: Detect user's language
        // FIXME: Error handling
        initial_langcode = "en-US".parse().unwrap();
    }

    initial_langcode
}

pub fn get_valid_languages() -> impl FnOnce() -> Vec<Language> {
    || {
        let langs = vec![ Language::from_str(EN_US), Language::from_str(JA_JP) ];

        let valid_langs = langs
            .into_iter()
            .filter_map(|lang| lang.ok())
            .collect::<Vec<_>>();

        valid_langs
    }
}
