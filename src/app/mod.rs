mod components;
pub mod consts;
mod i18n;
mod router;

use dioxus::prelude::*;
use dioxus_router::prelude::*;
use dioxus_std::i18n::use_init_i18n;
pub(crate) use router::Route;

pub fn App(cx: Scope) -> Element {
    log::info!("Starting app!");

    let initial_langcode = i18n::get_langcode();

    use_init_i18n(
        cx,
        initial_langcode,
        "en-US".parse().unwrap(),
        i18n::get_valid_languages(),
    );

    cx.render(rsx! {
        link {
            href: "https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200",
            rel: "stylesheet"
        }
        link { href: "tailwind.css", rel: "stylesheet" }
        Router::<Route> {}
    })
}
