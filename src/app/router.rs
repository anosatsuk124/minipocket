use super::components::{home::Home, settings::Settings};

use dioxus::prelude::*;
use dioxus_router::prelude::*;

#[derive(Routable, PartialEq, Debug, Clone)]
pub enum Route {
    #[route("/")]
    Home {},
    #[route("/settings")]
    Settings {},
}
