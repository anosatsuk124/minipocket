#![allow(non_snake_case)]
mod app;
pub(crate) use app::consts::*;
use app::App;

#[cfg(not(target_arch = "wasm32"))]
pub fn run_desktop() {
    drop(env_logger::try_init());

    dioxus_desktop::launch(App);
}

#[cfg(target_arch = "wasm32")]
pub fn run_web() {
    wasm_logger::init(wasm_logger::Config::default());
    console_error_panic_hook::set_once();

    dioxus_web::launch(App);
}
