use minipocket::*;

fn main() {
    #[cfg(not(target_arch = "wasm32"))]
    run_desktop();

    #[cfg(target_arch = "wasm32")]
    run_web();
}
